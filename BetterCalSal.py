#!python2
import sys
from PySide import QtGui, QtCore


class winMain(QtGui.QWidget):

    def __init__(self):
        super(winMain, self).__init__()
        self.initUI()
        
    def initUI(self):
        
        # STYLES
        self.setWindowTitle('BetterCalSal')
        self.setGeometry(900,300,300,300)
        
        labStyle = "QLabel {font-size: 14pt}"
        lineEditStyle = "QLineEdit {font-size: 24pt}"

        self.setStyleSheet(labStyle + lineEditStyle)
        
        # DEFINE ELEMENTS
        layout = QtGui.QVBoxLayout()
        
        self.labTax = QtGui.QLabel('Tax')
        self.labHrs = QtGui.QLabel('Hours/Week')
        self.labGro = QtGui.QLabel('Gross (without taxes)')
        self.labMon = QtGui.QLabel('Monthly')
        self.labBiW = QtGui.QLabel('Bi-Weekly')
        self.labDay = QtGui.QLabel('Daily')
        self.labHou = QtGui.QLabel('Hourly')
        
        self.labHrs.setToolTip("affects only hourly rate")
        
        self.textTax = QtGui.QLineEdit()
        self.textHrs = QtGui.QLineEdit()
        self.textGro = QtGui.QLineEdit()
        self.textMon = QtGui.QLineEdit()
        self.textBiW = QtGui.QLineEdit()
        self.textDay = QtGui.QLineEdit()
        self.textHou = QtGui.QLineEdit()
        
        # ELEMENTS DEFAULTS
        self.textTax.setFixedWidth(55)
        self.textTax.setText('37')
        self.textHrs.setFixedWidth(55)
        self.textHrs.setText('40')
        
        # ELEMENTS CONNECT
        self.textTax.textEdited.connect(lambda: self.proc('textTax'))
        self.textHrs.textEdited.connect(lambda: self.proc('textHrs'))
        self.textGro.textEdited.connect(lambda: self.proc('textGro'))
        self.textMon.textEdited.connect(lambda: self.proc('textMon'))
        self.textBiW.textEdited.connect(lambda: self.proc('textBiW'))
        self.textDay.textEdited.connect(lambda: self.proc('textDay'))
        self.textHou.textEdited.connect(lambda: self.proc('textHou'))
        
        # ADD TO LAYOUT
        layout.addWidget(self.labTax)
        layout.addWidget(self.textTax)
        layout.addWidget(self.labHrs)
        layout.addWidget(self.textHrs)
        layout.addWidget(self.labGro)
        layout.addWidget(self.textGro)
        layout.addWidget(self.labMon)
        layout.addWidget(self.textMon)
        layout.addWidget(self.labBiW)
        layout.addWidget(self.textBiW)
        layout.addWidget(self.labDay)
        layout.addWidget(self.textDay)
        layout.addWidget(self.labHou)
        layout.addWidget(self.textHou)
        
        # SET AND SHOW
        self.setLayout(layout)
        self.show()
        
        
    def proc(self, opt):
    
        hours = int(self.textHrs.text())
        tax = int(self.textTax.text())
        try:
            gr = int(self.textGro.text())
        except:
            pass
        taxInv = (100.0-tax)/100
        
        
        if opt == 'textGro': gr = float(self.textGro.text())
        if opt == 'textMon': gr = float(self.textMon.text()) * 12 / taxInv
        if opt == 'textBiW': gr = float(self.textBiW.text()) * 26 / taxInv
        if opt == 'textDay': gr = float(self.textDay.text()) * 260 / taxInv
        if opt == 'textHou': gr = float(self.textHou.text()) * 260 * (hours/5) / taxInv
        
        #MATH
                
        net = gr * taxInv
        netMon = net /12
        netBiW = net /26
        netDay = net /260
        netHou = net /260/(hours/5)
            
        #SET VALUES
        if opt != 'textTax': self.textTax.setText(str(int(tax)))
        if opt != 'textHrs': self.textHrs.setText(str(int(hours)))
        if opt != 'textGro': self.textGro.setText(str(int(gr)))
        if opt != 'textMon': self.textMon.setText(str(int(netMon)))
        if opt != 'textBiW': self.textBiW.setText(str(int(netBiW)))
        if opt != 'textDay': self.textDay.setText(str(int(netDay)))
        if opt != 'textHou': self.textHou.setText(str(int(netHou)))

        
def main():
    app = QtGui.QApplication(sys.argv)
    win = winMain()
    sys.exit(app.exec_())
    
main()